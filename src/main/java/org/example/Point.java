package org.example;

public record Point(int x, int y) {

    // factory method is ok
    public static Point atLocation(int x, int y) {
        return new Point(x, y);
    }

    public boolean isAbove(Point that) {
        return this.x == that.x && this.y > that.y;
    }

    public static Point ORIGIN = new Point(0, 0);
}
