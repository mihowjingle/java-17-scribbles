package org.example;

public class Main {

    static Result<Integer> get(boolean success) {
        if (success) {
            return new Result.Success<>(2);
        } else {
            return new Result.Error<>("Whoops!");
        }
    }

    public static void main(String[] args) {
        final var person = new Person("Jan", "Kowalski");
        System.out.println(person.fullName());

        Result<Integer> x = get(true);
        System.out.println("x = " + x);
        Result<Integer> y = get(false);
        System.out.println("y = " + y);

        switch (person.fullName()) {
            case "Jan Kowalski", "Stefan Kowalski" -> System.out.println(person.fullName());
            case "Janina Kowalska" -> System.out.println(person.firstName());
                //default -> throw new IllegalStateException("Unexpected value: " + person.fullName());
        }

        final var asExpression = switch (person.fullName()) {
            case "Jan Kowalski", "Stefan Kowalski" -> person.fullName();
            case "Janina Kowalska" -> person.firstName();
            default -> {
                var name = person.fullName();
                yield name + " not found";
            }
        };

        System.out.println("As expression: " + asExpression);

        //////////////////////////

        if (x instanceof Result.Success<?> success) {
            System.out.println("success: " + success.value());
        } else if (x instanceof Result.Error<?> error) {
            System.out.println(error.errorMessage());
        }

        x.ifSuccess(value -> {
            int inc = value + 1;
            System.out.println("success + 1 = " + inc);
        });

        x.ifError(error -> System.out.println("won't see this"));
        x.ifError(System.out::println);
        x.ifSuccess(System.out::println);

        y.ifError(errorMessage -> {
            System.out.println("logging the error, saving alert into db, sending email to architect, notifying nasa");
            System.out.println("error: " + errorMessage);
        });

        // preview in 17:
        //switch (y) {
        //    case Result.Success success -> System.out.println(success.value());
        //    case Result.Error error -> System.out.println(error.errorMessage());
        //}

        //////////////////////////

        final var center = Point.atLocation(0, 0);
        boolean above = Point.atLocation(0, 3).isAbove(center);
        var message = """
                point at x = 0 and y = 3 is %s center
                """.formatted(above ? "above" : "not above");
        System.out.println(message);
    }
}
