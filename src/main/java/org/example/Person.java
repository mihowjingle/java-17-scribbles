package org.example;

public record Person(String firstName, String lastName) {

    // constructor stuff
    public Person {
        lastName = lastName.toUpperCase();
    }

    public String fullName() {
        return firstName + " " + lastName;
    }
}
