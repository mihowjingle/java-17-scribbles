package org.example;

import java.util.function.Consumer;

public sealed class ResultButClass<T> {

    static final class Success<Y> extends ResultButClass<Y> {
        private final Y value;

        public Success(Y value) {
            this.value = value;
        }

        public Y value() {
            return value;
        }
    }

    static final class Error<Y> extends ResultButClass<Y> {
        private final String errorMessage;

        public Error(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public String errorMessage() {
            return errorMessage;
        }
    }

    public void ifSuccess(Consumer<T> consumer) {
        if (this instanceof Success<T> success) {
            consumer.accept(success.value);
        }
    }

    public void ifError(Consumer<String> consumer) {
        if (this instanceof Error<?> error) {
            consumer.accept(error.errorMessage);
        }
    }
}
