package org.example;

import java.util.function.Consumer;
import java.util.function.Supplier;

// for when you only care if it was successful OR there was (any) exception
// still... why not try/catch? dunno, this is just a sandbox
public sealed interface Unsafe<T> {
    record Success<Y>(Y value) implements Unsafe<Y> {}
    record Error<Y>(Exception exception) implements Unsafe<Y> {}

    default void ifSuccess(Consumer<T> consumer) {
        if (this instanceof Success<T> success) {
            consumer.accept(success.value);
        }
    }

    default void ifError(Consumer<Exception> consumer) {
        if (this instanceof Error<?> error) {
            consumer.accept(error.exception);
        }
    }

    static <R> Unsafe<R> operation(Supplier<R> supplier) {
        try {
            return new Success<>(supplier.get());
        } catch (Exception exception) {
            return new Error<>(exception);
        }
    }

    static void main(String[] args) {
        Unsafe<Integer> result = Unsafe.operation(() -> {
            //if (1 == 1) throw new IllegalStateException("oh no");
            return 5;
        });
        if (result instanceof Success<Integer> success) {
            int ten = success.value + 5;
            System.out.println("ten = " + ten);
        } else if (result instanceof Error<?> error) {
            // would be nice if it was exhaustive (we know that if it's not Success, it's Error, because sealed and stuff)
            System.out.println(error.exception.getMessage());
        } else {
            System.out.println("won't see this");
        }

        ///////////////////////////////

        Unsafe.operation(() -> {
            //if (1 == 1) throw new IllegalStateException("oh no");
            return 6;
        }).ifSuccess(number -> {
            int x = number + 9;
            System.out.println("fifteen = " + x);
        });
    }
}
