package org.example;

import java.util.function.Consumer;

// for http responses, validation and stuff
public sealed interface Result<T> { // can also "permit" other classes, from other .java files
    record Success<Y>(Y value) implements Result<Y> {}
    record Error<Y>(String errorMessage) implements Result<Y> {}

    default void ifSuccess(Consumer<T> consumer) {
        if (this instanceof Success<T> success) {
            consumer.accept(success.value);
        }
    }

    default void ifError(Consumer<String> consumer) {
        if (this instanceof Error<?> error) {
            consumer.accept(error.errorMessage);
        }
    }
}
